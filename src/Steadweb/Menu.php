<?php

namespace Steadweb;

use Steadweb\MenuInterface;
use \ArrayAccess;
use \Iterator;

class Menu implements Iterator, ArrayAccess, MenuInterface
{
	/**
	 * @var assoc array of items
	 */
	protected $items = array();

	/**
 	 * @var flattened out array of items
	 */
	protected $flat = array();
	
	/**
	 * Template how the menu should look (default)
	 *
	 * @var array
	 */
	protected $template = array(
		'menu_open' => '<ul>',
		'menu_close' => '</ul>',
		'menu_item_open' => '<li>',
		'menu_item_close' => '</li>',
	);	
	
	/**
	 * @var set the array separator, default = . (dot) notation 
	 */
	protected static $separator = '.';
	
	/**
	 * Construct new menu. Allow dev to choose template.
	 *
	 */
	public function __construct($config = array())
	{
		if(isset($config['template']))
		{
			$this->template = $config['template'];
		}
	}

	/**
	 * Adds an item to the current menu instance. The param expected
	 *
	 * @param 		$item	MenuItemInterface
	 * @returns		object
	 */
	public function addItem(MenuItemInterface $item)
	{	
		// Append items to array and set value as array.
		$this->setItem($item->getLabel(), $item); 
		
		return $this;
	}

	/**
	 * Fetches an item by name. If one cannot be found, null is returned.
	 *
	 * @param		$name 	string
	 * @returns		object|null
	 */
	public function getItem($name)
	{	
		if($item = $this->findItem($this->flat, $name))
		{			
			return $item;
		}
		
		return null;
	}
	
	/**
	 * Sets an item using the name passed.
	 *
	 * @param		$name	string
	 * @param		$item	MenuItemInterface
	 * @returns		object
	 */
	public function setItem($name, MenuItemInterface $item)
	{
		// Reference the current list of items
		$array = &$this->items;
	
		// Build up a list of keys.
		$keys = is_array($name) ? $name : explode('.', $name);	
		$current_key = array();
		
		if(isset($this->flat[$name]))
		{
			$this->flat[$name] = $item;
			return true;
		}
		
		while(count($keys) > 1)
		{
			$key = array_shift($keys);		

			// Build up the current key as we go along
			// so we can dynamically create on if it 
			// doesn't already exist.
			$current_key[] = $key;			

			if ( ! isset($array[$key]) or ! is_array($array[$key]))
			{
				// On the fly, create the middle element, if it doesn't exist.
				$this->flat[implode(static::$separator, $current_key)] = new \Steadweb\Menu\Item($key);
				
				// Set as a blank array
				$array[$key] = array();
			}
			
			$array =& $array[$key];
		}

		// Set the newly position correctly as an empty array.
		$array[array_shift($keys)] = array();
		
		// Finally set the flat MenuItemInterface instance
		// to the flat array, for later use.
		$this->flat[$name] = $item; 
	}
	
	/**
	 * Reset the whole instance
	 *
	 * @returns object
	 */
	public function reset($wipe = false)
	{
		// if wiped, any other instances will will be cleared.
		if($wipe)
		{
			$this->items = array();
			$this->flat  = array();
		}
	
		// return a new instance of self
		return new self();
	}
	
	/**
	 * Specific to this class. Find the menu item based on the pointer and 
	 * separator.
	 *
	 * @returns	boolean
	 */
	private function findItem(array $items, $name)
	{				
		if( array_key_exists($name, $items))
		{		
			return $items[$name];
		}
		
		// Nothing found.
		return false;
	}

	private function findItemByUrl($uri)
	{
		foreach($this->flat as $arrkey => $item)		
		{
			// Exact match, do something.
			if(property_exists($item, 'uri') && ltrim($item->uri, "/") === $uri)
			{
				$item->keys = explode(".", $arrkey);
				$item->key = $arrkey;
				$item->menu = count($item->keys) > 1 ? $this->getSub($item->keys[0]) : null;

				return $item;
			}
		}

		return false;
	}

	public function activeItem($override = null)
	{
		$url = $override ? $override : $_SERVER['REQUEST_URI'];
		$path = ltrim($url, '/');
		$path = rtrim($path, '/');

		return $this->findItemByUrl($path, true);
	}

	public function activeItemOrFirst($override = null)
	{
		if( $item = $this->activeItem($override))
		{
			return $item;
		}

		return $this->findItem($this->flat, key($this->items));
	}

	/**
	 * Find all active segments
	 * 
	 * @return array
	 */
	public function activeSegments()
	{
		// Potential active URLs
		$active = array();

		// First, prep the REQUEST_URI
		$raw_path = ltrim($_SERVER['REQUEST_URI'], '/');
		$raw_path = rtrim($raw_path, '/');
		$segments = explode("/", $raw_path);

		// Find the active URL.
		for($i = 1; $i <= count($segments); $i++)
		{
			// Find the active item using active paths.
			$active[] = rtrim(implode("/", array_slice($segments, 0, $i)), "/");
		}

		return $active;
	}
	
	/**
	 * Render the menu with all items.
	 *
	 * @returns string
	 */
	public function render($items, $prefix = null)
	{	
		$content = $this->template['menu_open'];
	
		foreach($items as $name => $item)
		{
			// Reset, every time.
			$active_class = null;

			$open = $this->template['menu_item_open'];

			// @todo hanndle all {{element}}
			// not here, else where.
			// Product a name/id field for the <a/>
			$id = strtolower(preg_replace('/[^\da-zA-Z]/i', '', $name));
			$open = str_replace("{{id}}", $id, $open);

			$content .= $open;

			if($menuItem = $this->findItem($this->flat, $prefix.$name))
			{
				// Start the output
				$output = $menuItem->render();

				// If the item is the item
				// Or the segment is part of an item that is active
				// set to active.
				if($menuItem === $this->activeItem() || in_array(ltrim($menuItem->uri, '/'), $this->activeSegments()))
				{
					// @todo, put this into config / template
					// allow others to override this.
					$active_class = 'active';
				}

				// Replace the {{active}} tag with the active_class
				$output = str_replace("{{active}}", $active_class, $output);
				$content .= $output;
			}
			
			// Render any other sub menu items recursively.
			// This will handle menus within menus.
			if(! empty($item))
			{
				$_prefix = $prefix ? $prefix.$name.static::$separator : $name . static::$separator;

				// Append the item and close the menu item off.
				$content .= $this->render($item, $_prefix);
			}

			$content .= $this->template['menu_item_close'];
		}
		
		$content .= $this->template['menu_close'];
		
		return $content;
	}

	/**
	 * Retun the menu root items
	 * Ability to override the config because this method
	 * returns a new instance of Steadweb\Menu.
	 * 
	 * @param  array  $config
	 * @return object 
	 */
	public function rootItems($config = array())
	{
		$root_menu = new static(array('template' => $this->template));

		foreach($this->items as $item => $children)
		{
			if($root_item = $this->findItem($this->flat, $item))
			{
				$root_menu->addItem($root_item);
			}
		}

		return $root_menu;
	}

	/**
	 * Get any sub menu from another.
	 *
	 * Ability to override the config because this method
	 * returns a new instance of Steadweb\Menu.
	 * 
	 * @param  string $name
	 * @param  array  $config
	 * @return object
	 */
	public function getSub($name, $config = array())
	{
		$sub_menu = new static(array('template' => $this->template));

		if(array_key_exists($name, $this->items))
		{
			foreach($this->items[$name] as $subitem => $children)
			{
				if($sub_menu_item = $this->findItem($this->flat, $name.static::$separator.$subitem))
				{
					$sub_menu->addItem($sub_menu_item);
				}
			}
		}

		return $sub_menu;
	}

	/**
	 * Ability to change template on the fly.
	 * 
	 * @param array $config
	 */
	public function setTemplate(array $config = array())
	{
		if( isset($config['template']))
		{
			$this->template = $config['template'];
		}

		return $this;
	}

	/**
	 * Get the total number of items within the menu.
	 * 
	 * @return int
	 */
	public function count($total = true)
	{
		return count($this->items);
	}
	
	/**
	 * Renders the item when stringified.
	 *
	 */
	public function __toString()
	{
		return $this->render($this->items);
	}	
	
	/**
	 * ArrayAccess offsetExists method. 
	 * 
	 * Allows object to be accessed
	 * like an array. Checks if the menu item exists.
	 *
	 * @returns boolean
	 */
	public function offsetExists($offset)
	{
		return $this->getItem($offset) ? true : false;
	}
	
	/**
	 * ArrayAccess offsetGet method. 
	 *
	 * Allows object to be accessed like an array.
	 * Returns the menu item if found.
	 *
	 * @returns object|mixed
	 */	
	public function offsetGet($offset)
	{
		return $this->getItem($offset);
	}
	
	/**
	 * ArrayAccess offsetSet method. 
	 *
	 * Allows object to be accessed like an array.
	 * Sets the menu item.
	 *
	 * @returns object|mixed
	 */		
	public function offsetSet($offset, $value)
	{
		return $this->setItem($offet, $value);
	}
	
	/**
	 * ArrayAccess offsetUnset method. 
	 *
	 * Allows object to be accessed like an array.
	 * Unsets the menu item, if found.
	 *
	 * @returns object|mixed
	 */	
	public function offsetUnset($offset)
	{
		return $this->removeItem($offset);
	}
	
	/**
	 * @todo implement
	 */
	public function next()
	{
	}
	
	/**
	 * @todo implement
	 */	
	public function mixed()
	{
	}
	
	/**
	 * @todo implement
	 */	
	public function key()
	{
	}
	
	/**
	 * @todo implement
	 */	
	public function current()
	{
	}
	
	/**
	 * @todo implement
	 */	
	public function valid()
	{
	}
	
	/**
	 * @todo implement
	 */	
	public function rewind()
	{
	}
}
