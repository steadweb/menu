<?php

namespace Steadweb;

interface MenuInterface
{
	/**
	 * Adds an item to the current menu instance. The param expected
	 *
	 * @param 		$item	MenuItemInterface
	 * @returns		object
	 */
	public function addItem(MenuItemInterface $item);
	
	/**
	 * Fetches an item by name. If one cannot be found, null is returned.
	 *
	 * @param		$name 	string
	 * @returns		object|null
	 */
	public function getItem($name);
	
	/**
	 * Sets an item using the name passed.
	 *
	 * @param		$name	string
	 * @param		$item	MenuItemInterface
	 * @returns		object
	 */
	public function setItem($name, MenuItemInterface $item);
	
	/**
	 * Outputs the menu as HTML.
	 *
	 * @returns		string
	 */
	public function render($items);
}
