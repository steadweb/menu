<?php

namespace Steadweb\Menu;

use Steadweb\MenuItemInterface;

class Item implements MenuItemInterface
{
	/**
	 * @var string
	 */
	protected $name;

	/**
	 * Menu items require a name and Uri.
	 *
	 */
	public function __construct($name)
	{
		$this->name = $name;
	}
	
	/**
	 * Attempt to the render the menu item.
	 *
	 * @returns string
	 */
	public function __toString()
	{
		try
		{
			return $this->render();
		}
		catch(\Exception $e)
		{
			return $e->getMessage();
		}
	}
	
	/**
	 * Get the label of the menu item.
	 *
	 * @returns string
	 */
	public function getLabel()
	{
		return $this->name;
	}
	
	/**
	 * Render the menu item.
	 *
	 * @returns string|mixed
	 */
	public function render()
	{
		return $this->name;
	}	
}