<?php

namespace Steadweb\Menu;

use Steadweb\MenuItemInterface;

abstract class ItemDecorator implements MenuItemInterface
{
	/**
	 * @var MenuItemInterface
	 */
	protected $item;

	/**
	 * Set $item to what ever is passed.
	 *
	 */
	public function __construct(MenuItemInterface $item)
	{
		$this->item = $item;
	}
	
	/**
	 * Get the name of the menu item.
	 *
	 * @returns string
	 */
	public function getName()
	{
		return $this->item->getLabel();
	}
	
	/**
	 * Attempt to the render the menu item.
	 *
	 * @returns string
	 */
	public function __toString()
	{
		try
		{
			return $this->item->render();
		}
		catch(\Exception $e)
		{
			return $e->getMessage();
		}
	}
	
	/**
	 * Basic magic method to set a property.
	 *
	 */
	public function __set($item, $value)
	{
		$this->{$item} = $value;
	}
	
	/**
	 * Basic magic method to get a property.
	 *
	 */	
	public function __get($item)
	{
		return $this->{$item};
	}
}