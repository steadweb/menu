<?php

namespace Steadweb\Menu\Item;

use Steadweb\Menu\ItemDecorator;

class Html extends ItemDecorator
{
	/**
	 * @var string
	 */
	protected $html = null;

	/**
	 * Render the menu item based.
	 *
	 * @returns string
	 */
	public function render()
	{
		return $this->html;
	}
	
	/**
	 * Return the label menu item label.
	 *
	 */
	public function getLabel()
	{
		return $this->getName();
	}	
}