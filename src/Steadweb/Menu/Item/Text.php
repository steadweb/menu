<?php

namespace Steadweb\Menu\Item;

use Steadweb\Menu\ItemDecorator;

class Text extends ItemDecorator
{
	/**
	 * @var string
	 */
	protected $uri = null;

	/**
	 * Render the menu item based.
	 *
	 * @returns string
	 */
	public function render()
	{
		return "<a class='{{active}}' href='{$this->uri}'>{$this->getLabel()}</a>";
	}
	
	/**
	 * Get the name of the link.
	 *
	 * @returns string
	 */
	public function getLabel()
	{
		return $this->getName();
	}	
}