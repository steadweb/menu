<?php

namespace Steadweb;

interface MenuItemInterface
{
	/**
	 * Set the label of the menu Item. This won't be used within the view, but within the
	 * structure.
	 *
	 */
	public function getLabel();
	
	/**
	 * Force the type of menu item to render it's own view. 
	 *
	 */
	public function render();
}
